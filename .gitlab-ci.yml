# Gitlab CI pipeline definition for `sample-helm-app`

# Note: environment variables starting with `CI_` are automatically set by Gitlab
# https://docs.gitlab.com/ee/ci/variables/predefined_variables.html

stages:
  - build-docker-image
  - deploy

####################
#     BUILDING     #
# Container Images #
####################

.build-docker-image:
  stage: build-docker-image
  image:
    name: gitlab-registry.cern.ch/ci-tools/docker-image-builder
    entrypoint: [""]
  variables:
    # If the location/name of the Dockerfile is different, this variable needs to be adjusted
    DOCKERFILE: $CI_PROJECT_DIR/Dockerfile
  script:
    # Prepare Kaniko configuration file
    - echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"PASSWORD\":\"$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
    # Build and push the image
    - /kaniko/executor --single-snapshot --context $CI_PROJECT_DIR --dockerfile "${DOCKERFILE}" --destination "${TO}"
    # Print the full registry path of the pushed image
    - echo "Image pushed successfully to ${TO}"

Build branch image:
  extends: .build-docker-image
  variables:
    # For feature branches, use the branch name as a `tag`
    TO: ${CI_REGISTRY_IMAGE}:${CI_COMMIT_REF_SLUG}
  rules:
    - if: $CI_COMMIT_BRANCH != 'master'

Build master image:
  extends: .build-docker-image
  variables:
    # For master branch, use the `latest` tag
    TO: ${CI_REGISTRY_IMAGE}:latest
  rules:
    - if: $CI_COMMIT_BRANCH == 'master'

####################
#     DEPLOYING    #
# Helm Deployments #
####################

.deploy-base:
  stage: deploy
  image:
    name: gitlab-registry.cern.ch/paas-tools/openshift-client:latest
  variables:
    IMAGE: ${CI_REGISTRY_IMAGE}:latest
    OPENSHIFT_SERVER: https://api.paas-stg.okd.cern.ch
  script:
    # Log in to Openshift and switch to the correct project
    - oc login --token="${OPENSHIFT_TOKEN}" --server="${OPENSHIFT_SERVER}"
    - oc project $NAMESPACE
    # Install or upgrade the Helm chart as necessary
    # --wait waits until all components have successfully started
    # --atomic and --cleanup-on-fail will roll back the deployment if unsuccessful
    # --values sets Helm values from a Gitlab CI variable
    # --set set a single Helm value
    - helm -n $NAMESPACE upgrade
      --wait
      --cleanup-on-fail
      --install
      --values "${HELM_SECRETS_FILE}"
      --set "image=${IMAGE}"
      --set "revision=${CI_COMMIT_SHA}"
      sample-helm-app
      ./chart/

Deploy staging:
  extends: .deploy-base
  variables:
    NAMESPACE: test-ber
  environment:
    name: staging
  rules:
    - if: $CI_COMMIT_BRANCH == 'master'
      when: manual

Deploy production:
  extends: .deploy-base
  variables:
    NAMESPACE: sample-helm-app-production
  environment:
    name: production
  rules:
    - if: $CI_COMMIT_BRANCH == 'master'
      when: manual
