# Sample app deployed on OKD4 with Helm

This repository contains a sample application that shows how to build a custom container image in Gitlab CI and deploy the container using a Helm chart on [CERN's OKD4 PaaS](https://paas.docs.cern.ch).

## Source code

The application source code is stored in `src/` and is written in Go.
The build process (described in the `Dockerfile`) might differ depending on the choice of programming language, but once the container is built it does not affect the deployment process.

## Deployment

*Helm charts* are the most common approach for deploying cloud-native applications on Kubernetes/OpenShift.
The Helm chart is stored in the `chart/` folder.
In a nutshell, Helm charts are defining a set of Kubernetes manifest templates which are evaluated as [Golang string templates](https://helm.sh/docs/chart_template_guide/function_list/) with parameters from [`values.yaml` as input](https://helm.sh/docs/chart_template_guide/values_files/).
For more details about Helm charts, please refer to the [upstream documentation](https://helm.sh/docs/topics/) and the [accompanying Getting Started Guide](https://helm.sh/docs/chart_template_guide/getting_started/).

**Note**: the deployment automation described in the next section can also be used when you are *NOT* using Helm - simply replace the `helm` commands with equivalent deployment commands.
For example, when using [Kustomize](https://kustomize.io/), use `oc apply -k ./manifests/` instead of `helm upgrade` in `.gitlab-ci.yml`.

## Automation

The `.gitlab-ci.yml` config files contains the Gitlab pipeline definition.

There are two main the sections:

### Building

The first section describes how the container image for the application is built.
By default, the [Dockerfile](https://docs.docker.com/engine/reference/builder/) (which describes the build process) is expected to be in the root of the project directory, but this can be changed by modifying the `DOCKERFILE` variable.
When the pipeline runs for a *feature branch* (i.e. `master`), it will push the container image to Gitlab's registry at `gitlab-registry.cern.ch/<PROJECT-NAME>:<BRANCH-NAME>`.
For pipeline on the `master` branch, it will push the image to `gitlab-registry.cern.ch/<PROJECT-NAME>:latest`.

*Note*: instead of using the Gitlab container registry, the [CERN's Harbor registry](https://kubernetes.docs.cern.ch/docs/registry/gitlab/) can be used as well, which offers more features (e.g. container security scanning).

### Deploying

The second section defines the deployment process.
The Helm chart from the `chart` folder is used together with the values from `HELM_SECRETS_FILE` (a Gitlab CI variable) to describe the desired deployment, which then gets installed/updated by the `helm` command-line tool.
For this step an [OpenShift deploy token](https://paas.docs.cern.ch/1._Getting_Started/7-permissions/#openshift-service-accounts) needs to be created.

The deployment describes two environments: `staging` and `production`.
Each environment has its *own* OpenShift project to ensure proper isolation.

While not recommended, it is also possible to deploy multiple environments within the same OpenShift project.
In this case, a different Helm chart name needs to be used for each installation and special care needs to be taken when writing the Helm chart (all Kubernetes resources *must* have unique names).

Note that these pipeline jobs are set to `manual`: to start the job (and begin the re-/deployment), you need to click on the `Trigger this manual action` button in Gitlab.

## Getting started

To try out this project yourself, the following steps are required:

1. Fork this GitLab project by clicking the button in the top-right corner
2. Create a Gitlab CI variable (Project `Settings` > `CI/CD` > `Variables`) called `HELM_SECRETS_FILE` of type `File` and Environment `staging` and fill it with the required Helm values
3. Create a new OpenShift project for your staging environment
3. Generate an OpenShift serviceaccount and token in the new project
4. Add another Gitlab CI variable called `OPENSHIFT_TOKEN` of type `Variable` and Environment `staging` and use the token generated in the previous step as the value
5. Trigger a new pipeline (`CI/CD` > `Pipelines` > `Run Pipeline`)
6. Wait until the *"Build image"* job finishes, then trigger the *"Deploy staging"* step
7. After the deployment finishes, return to the OpenShift project and ensure a pod is running and you can access the application at the specified hostname
