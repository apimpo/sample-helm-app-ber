package main

import (
    "fmt"
    "html"
    "log"
    "net/http"
)

// https://tutorialedge.net/golang/creating-simple-web-server-with-golang/

func rootHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Hello, %q!", html.EscapeString(r.URL.Path))
}

func healthHandler(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
	fmt.Fprintf(w, "OK")
}

func main() {

    http.HandleFunc("/", rootHandler)

    http.HandleFunc("/-/healthy", healthHandler)

	// Note: make sure the server is listening on ALL addresses, NOT just the loopback interface (localhost)
    log.Fatal(http.ListenAndServe(":8080", nil))
}
